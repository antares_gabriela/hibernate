package hibernate.antares.com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Principal {

	protected SessionFactory sessionFactory;

	protected void setup() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure() // configures settings
																									// from
																									// hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}

	protected void exit() {
		sessionFactory.close();
	}

	protected void create() {
		Clients cliente = new Clients();
	    
		cliente.setCodiCli(50);
		cliente.setNomCli("Antonio Aranda");
		cliente.setCiutat("Tortosa");
		cliente.setNif("74495503E");
	 
	    Session session = sessionFactory.openSession();
	    session.beginTransaction();
	 
	    session.save(cliente);
	 
	    session.getTransaction().commit();
	    session.close();
	}

	protected void read() {
		 Session session = sessionFactory.openSession();
		 
		    int codiCli = 10;
		    Clients cliente = session.get(Clients.class, codiCli);
		 
		    System.out.println("Nombre: " + cliente.getNomCli());
		    System.out.println("DNI: " + cliente.getNif());
		    System.out.println("Telefono: " + cliente.getTelefon());
		 
		    session.close();
	}

	protected void update() {
		Clients cliente = new Clients();
	    cliente.setCodiCli(50);
	    cliente.setNomCli("Teresa Domenech");
	    cliente.setNif("67463098G");
	    		
	 
	    Session session = sessionFactory.openSession();
	    session.beginTransaction();
	 
	    session.update(cliente);
	 
	    session.getTransaction().commit();
	    session.close();
	}

	protected void delete() {
		Clients cliente = new Clients();
		cliente.setCodiCli(50);
		cliente.setNomCli("cualquiera porque es necesario que haya uno");
	 
	    Session session = sessionFactory.openSession();
	    session.beginTransaction();
	 
	    session.delete(cliente);
	 
	    session.getTransaction().commit();
	    session.close();
	}

	public static void main(String[] args) {
		Principal manager = new Principal();
		manager.setup();
		
		//manager.create();
		//manager.read();
		//manager.update();
		manager.delete();

		manager.exit();

	}

}
